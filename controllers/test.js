var app = angular.module('angapp', []);

app.controller('testcontroller', ['$scope', 'services/somefactory', function($scope, somefactory) {
	$scope.text = "helloworld " + somefactory;
}]);

app.directive('somedirective', function($scope) {
	return {};
});

app.factory('somefactory', function() {
	return function() {
	};
});
