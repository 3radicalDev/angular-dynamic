(function () {


	var required = {};
	var STRIP_COMMENTS = /((\/\/.*$)|(\/\*[\s\S]*?\*\/))/mg;
	var ARGUMENT_NAMES = /([^\s,]+)/g;
	var old = { modules: {} };
	var modules = {};
	var loaded = {};
	var completeFunctions = [];
	var wrapped = {};
	var booted;
	old.module = angular.module;
	var $controllerProvider, $filterProvider, $compileProvider, $provide;


	angular.module = function overloadModule(name, requirements) {
		loaded[name] = true;
		if (wrapped[name]) {
			addRequirements(name, requirements);
			return window.currentApp = wrapped[name];
		}
		var module = modules[name] = modules[name] || old.module.apply(angular,
			simpleArgs(arguments));

		module.config([
			'$provide', '$httpProvider', '$controllerProvider', '$filterProvider', '$compileProvider', function ($p, $httpProvider, $cp, $fp, $comp) {
				$provide = $p;
				$controllerProvider = $cp;
				$filterProvider = $fp;
				$compileProvider = $comp;
				$provide.factory('httpchecker', [
					'$q', function ($q) {
						return {
							response: function (response) {

								var defer = $q.defer();
								if (response.config.url.indexOf('.html') === -1) {
									defer.resolve(response);
								}
								else {

									var fragment = document.createDocumentFragment();
									var div = document.createElement('div');
									div.innerHTML = response.data;
									fragment.appendChild(div);
									var controllers = Array.prototype.slice.call(fragment.querySelectorAll('[ng-controller]'),
										0).map(function (c) {
											return c.getAttribute('ng-controller');
										}).filter(function (c) {
											return !loaded[c] && !required[c];
										});
									for (var i in controllers) {
										if (controllers.hasOwnProperty(i)) {
											required[controllers[i]] = {
												dependsOn: {},
												dependents: {},
												hint: parent(response.config.url) + '/controllers'
											};
										}
									}
									if (controllers.length) {
										markForProcessing();
										completeFunctions.push(function () {
											if (controllers.every(function (c) {
													return loaded[c];
												})) {
												setTimeout(function () {
													defer.resolve(response);
												}, 0);
												return true;
											}
											else {
												return false;
											}
										});
									}
									else {
										defer.resolve(response);
									}

								}
								return defer.promise;

							}
						};
					}
				]);
				$httpProvider.interceptors.push('httpchecker');

			}
		]);
		addRequirements(name, requirements);
		required[name].fn = function () {
			/* Got the module */
		};
		required[name].dontLoad = true;
		return window.currentApp = wrapped[name] = wrapModule(old.modules[name] = { original: module });
	};
	old.bootstrap = angular.bootstrap;

	angular.bootstrap = function (document, requirements) {
		var args = simpleArgs(arguments);

		addRequirements('boot', requirements);
		required['boot'].fn = function () {
			booted = true;
			loaded['boot'] = true;
			old.bootstrap.apply(angular, args);
			var $rootScope = angular.element(document).injector().invoke([
				'$rootScope', function ($rootScope) {
					return $rootScope;
				}
			]);
			$rootScope.$on('$includeContentLoaded', function () {
				markForProcessing();
			});
		};


	};


	function simpleArgs(args) {
		args = Array.prototype.slice.call(args, 0).map(function (a) {
			return typeof a == 'string' && a.indexOf('/') >= 0 ? a.substring(a.lastIndexOf('/') + 1) : (Array.isArray(a) ? simpleArgs(a) : a);
		});
		return args;
	}

	function getParamNames(func) {
		var fnStr = func.toString().replace(STRIP_COMMENTS, '');
		var result = fnStr.slice(fnStr.indexOf('(') + 1, fnStr.indexOf(')')).match(ARGUMENT_NAMES);
		if (result === null) {
			result = [];
		}
		return result;
	}

	var loading = {};

	function parent(path) {
		var idx = path.lastIndexOf('/');
		if (idx == -1) {
			return "";
		}
		return path.substring(0, idx);
	}

	function load(name, oncomplete, onerror) {
		oncomplete = oncomplete || function () {
		};
		onerror = onerror || function () {
		};
		if (loading[name]) {
			return;
		}
		var loader = loading[name] = {
			path: parent(window.location.pathname),
			use_hint: true,
			try_root: false,
			try_short: false
		};
		var req = required[name];

		function loaded() {
			console.log("LOADED ****> " + name);
			oncomplete();
		}


		function tryNext() {
			if (required[name].dontLoad) {
				return;
			}
			if (lastScript) {
				document.head.removeChild(lastScript);
				lastScript = null;
			}
			if (loader.use_hint && req.hint) {
				if (!loader.try_root && req.hint && loader.use_hint && !loader.try_short) {
					loadFrom(loader.path + '/' + req.hint + '/' + name + '.js');
					loader.try_short = true;
				}
				else {
					loadFrom(loader.path + '/' + req.hint + '/' + name.replace(/controller|directive|factory|filter/g,
						'') + '.js');

					loader.try_short = false;
					loader.use_hint = false;
				}
			}
			else {
				if (!loader.try_root) {
					if (!loader.try_short) {
						loadFrom(loader.path + '/' + name + '.js');
						loader.try_short = true;

					}
					else {
						loadFrom(loader.path + '/' + name.replace(/controller|directive|factory|filter/g, '') + '.js');
						loader.try_short = false;
						loader.try_root = true;

					}
				}
				else {
					var idx = loader.path.lastIndexOf('/');
					if (idx == -1) {
						onerror();
						return;
					}
					loader.try_root = false;
					loader.use_hint = true;
					loader.path = parent(loader.path);
					tryNext();
				}
			}
		}

		tryNext();
		var lastScript;

		function loadFrom(path) {
			console.log("load from " + path);
			var script = document.createElement('script');
			script.src = path;
			if (script.addEventListener) {

				script.addEventListener('load', loaded);
				script.addEventListener('error', tryNext);
				script.addEventListener('undefined', tryNext);
			}
			else if (script.readyState) {
				script.onreadystatechange = loaded;
			}
			lastScript = script;
			document.head.appendChild(script);
		}

	}

	var processorId = 0;
	var ready = document.readyState === 'complete';

	window.addEventListener('load', function () {
		ready = true;
		var auto = document.querySelector('[ng-app-load]');
		if (auto) {
			setTimeout(function () {
				angular.bootstrap(document, auto.getAttribute('ng-app-load').split(','));
			}, 0);
		}
	});

	function markForProcessing() {
		clearTimeout(processorId);
		processorId = setTimeout(resolve, 0);
	}

	function count(item, ignore) {
		if (!item) {
			return 0;
		}
		var count = 0;
		for (var i in item) {
			if (item.hasOwnProperty(i) && i != ignore) {
				if (!loaded[i]) {
					count += 1;
				}
			}
		}
		return count;
	}

	/* Resolve the dependencies and load */
	function resolve() {
		var i;
		processorId = 0;
		if (!ready) {
			markForProcessing();
			return;
		}

		var controllers = Array.prototype.slice.call(document.querySelectorAll('[ng-controller]'), 0).map(function (c) {
			return c.getAttribute('ng-controller');
		}).filter(function (c) {
			return !loaded[c] && !required[c];
		});
		for (i in controllers) {
			if (controllers.hasOwnProperty(i)) {
				required[controllers[i]] = {
					dependsOn: {},
					dependents: {},
					hint: 'controllers'
				};
			}
		}

		var repeat = true;
		while (repeat) {
			repeat = false;
			for (i in required) {
				if (required.hasOwnProperty(i)) {
					var item = required[i];
					if (count(item.dependsOn, i) == 0) {
						if (item.fn) {
							/* actually do the load */
							loaded[i] = true;
							item.fn();
							delete required[i];
							repeat = true;
						}
						else {
							if (!loaded[i]) {
								/* Try to dynamically load it */
								load(i);
							}
							else {
								delete required[i];
							}

						}

					}

				}
			}
		}

		if (count(required) == 0) {
			var evt = new Event('angularload');
			document.dispatchEvent(evt);
			for (i = completeFunctions.length - 1; i >= 0; i--) {
				try {
					var ok = completeFunctions[i]();
					if (ok) {
						completeFunctions.splice(i, 1);
					}

				}
				catch (e) {
				}
			}
		}


	}

	function addRequirements(name, dependencies) {
		required["boot"] = required["boot"] || {
			dependsOn: {},
			dependencies: {}
		};
		required["boot"].dependsOn[name] = true;
		var main = required[name] = required[name] || {
			dependsOn: {},
			dependents: {}
		};
		var dependentOn = [];
		if (Array.isArray(dependencies)) {
			dependentOn = dependencies.filter(function (d) {
				return typeof d == 'string' && d.substring(0, 1) != '$';
			});
		}
		else {
			dependentOn = getParamNames(dependencies).filter(function (a) {
				return a.substring(0, 1) != '$';
			});
		}
		for (var i = 0; i < dependentOn.length; i++) {
			var parts = dependentOn[i].split('/');
			var dependency = parts[parts.length - 1];
			if (loaded[dependency]) {
				continue;
			}
			var hint = parts.length > 1 ? parts.slice(0, -1).join('/') : '';
			main.dependsOn[dependency] = true;
			var item = required[dependency] = required[dependency] || {
				dependsOn: {},
				dependents: {},
				hint: hint
			};
			/* Increment the number of dependencies */
			var dependent = item.dependents[name] = item.dependents[name] || { count: 0 };
			dependent.count += 1;
		}
		required[name] = required[name] || { dependents: {} };
		markForProcessing();

	}

	function requireComponents(wrapped, module, hint, $provider, method) {
		method = method || 'register';
		hint = hint || '';
		return function fn(name, dependencies) {
			/* This function intercepts angular */
			var main = required[name] = required[name] || {
				dependsOn: {},
				dependents: {},
				hint: hint
			};
			var args = simpleArgs(arguments);
			main.fn = main.fn || function () {
				if (!booted || !$provider) {
					wrapped.apply(module, args);
				}
				else {
					$provider()[method].apply(module, args);
				}
			};

			addRequirements(name, dependencies);
		};
	}

	function wrapModule(ang) {
		var module = ang.original;
		ang.controller = module.controller.bind(module);
		ang.factory = module.factory.bind(module);
		ang.directive = module.directive.bind(module);
		ang.service = module.service.bind(module);
		ang.filter = module.filter.bind(module);
		ang.value = module.value.bind(module);


		module.controller = requireComponents(ang.controller, module, '', function() { return $controllerProvider; });
		module.factory = requireComponents(ang.factory, module, '', function() { return $provide; }, 'factory');
		module.directive = requireComponents(ang.directive, module, '', function() { return $compileProvider; }, 'directive');
		module.service = requireComponents(ang.service, module, '', function() { return $provide;}, 'service');
		module.filter = requireComponents(ang.filter, module, '', function() { return $filterProvider; });
		module.value = requireComponents(ang.value, module, '', function() { return $provide; }, 'value');

		return module;
	}

})();
